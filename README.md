<p align="center">
    <img  alt="Growth" title="#Growth" src="src/assets/bannerMobileVersion.svg" />
</p>

<h1 align="center"> 
	Growth: AgroTreinamentos online 🌾 
</h1>
<p align="center"> 🌿 Plataforma de treinamentos online focado no Agro.  🌱 </p>

<img src="https://img.shields.io/badge/mobile-react%20native-blueviolet">
<img src="https://img.shields.io/badge/node-12.17.0-green">
<img src="https://img.shields.io/badge/backend-firebase-orange">

<h3 align="center"> 
	🚧 Status do projeto🚧
</h3>

<p align="center">
  <img src="https://img.shields.io/badge/versão-1.0.0-green"> - production <br>
  <img src="https://img.shields.io/badge/versão-1.0.1-yellow"> - under construction
</p>

<h2> 💻 Acesse o projeto <h2>

<a href="https://bit.ly/2W5PlY1">
  <img src="https://img.shields.io/badge/debug%20app-android-green">
</a>


Tabela de conteúdos
=================
<!--ts-->
   * [Sobre o projeto](#-sobre-o-projeto)
   * [Principais benefícios](#-principais-benef%C3%ADcios)
   * [Funcionalidades](#%EF%B8%8F-funcionalidades-web)
   * [Layout](#-layout)
   * [Como executar o projeto](#-como-executar-o-projeto)
      * [Rodando o projeto](#-rodando-o-projeto)
   * [Tecnologias](#-tecnologias)
   * [Autores](#-autores)
   * [Licença](#-licença)
<!--te-->

## 📋 Sobre o projeto
O Growth é uma plataforma de treinamentos online pensada para atender o Agro. Composta por aplicativo mobile e ambiente web, proporciona um espaço para centralização de tutoriais sobre as ferramentas e auxilia no processo de replicação de treinamentos entre os usuários.

O projeto é composto por 3 repositórios que se complementam, abrangendo assim ambiente web e mobile. 

## 📊 Principais benefícios
Projeto desenvolvido tendo como principais benefícios:
- Disponibilidade
- Objetividade
- Agilidade
- Segurança
- Padronização
- Escalabilidade
---

## ⚙️ Funcionalidades mobile

- [x] Autenticação de usuários
  - [x] Esqueci senha
- [x] Usuários
  - [x] Autocadastro de usuários
  - [x] Edição de Perfil
- [x] Dashboard
- [x] Acessar, iniciar e finalizar Treinamentos
- [x] Acessar, iniciar e finalizar Treinamentos
- [x] Pesquisar treinamentos
- [x] Pesquisar Trilhas


## 🎨 Layout

O layout da aplicação está disponível no Figma:

<a href="https://www.figma.com/file/GViniyZf1rx5PhumIIhNyK/Mobile?node-id=180%3A838">
  <img src="https://img.shields.io/badge/Acessar%20Layout%20-Figma-%2304D361">
</a>

## 📦 Como executar o projeto

####  🛠  Pré Requisitos

Importante ter configurado ambiente de desenvolvimento com os seguintes elementos: 

<kbd>Node</kbd> 
<kbd>Java</kbd> 
<kbd>Android Studio</kbd>
<kbd>Xcode</kbd>


💡 É possível desenvolver o projeto utilizando windows, macOS ou Linux como sistema operacional, contudo para realizar a build da versão iOS é obrigatório o uso de macOS.

#### 🎲 Rodando o projeto - Android


```bash

# Clone este repositório
$ git clone git@gitlab.com:senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/app.git

# Acesse a pasta do projeto no terminal/cmd
$ cd app

# Instale as dependências
$ yarn install

# Execute a aplicação em modo de desenvolvimento
$ yarn android

# A aplicação será aberta na porta:3000 - acesse http://localhost:3000

```

#### 🎲 Rodando o projeto - iOS


```bash

# Clone este repositório
$ git clone git@gitlab.com:senac_pos-tcc_desenvolvimento-web-mobile-2019/jl/web.git

# Acesse a pasta do projeto no terminal/cmd
$ cd app

# Instale as dependências
$ yarn install

# Acesse a pasta de arquivos ios
$ cd ios 

# Instale as dependências do projeto ios
$ pod install

# Retorne para a pasta anterior
$ cd ..

# Execute a aplicação em modo de desenvolvimento
$ yarn ios


```

## 🕹 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto mobile:

-   **[React Native](https://reactnative.dev/)**
-   **[React Navigation](https://reactnavigation.org/)**
-   **[FontAwesome](https://fontawesome.com/)**
-   **[React i18next](https://react.i18next.com/)**
-   **[Firebase](https://firebase.google.com/)**


## 🦸 Autores

<table>
  <tr>
    <td align="center"><a href="https://gitlab.com/lekkinhah"><img style="border-radius: 50%;" src="https://pt.gravatar.com/userimage/186334662/ec308d4832e83fdc97fbb724d6f69a70.jpg" width="100px;" alt=""/><br /><sub><b>Letícia Vargas</b></sub></a><br /> </td>
    <td align="center"><a href="https://gitlab.com/jeferson.oliveira"><img style="border-radius: 50%;" src="https://pt.gravatar.com/userimage/16093557/f396bcb89e84b8e858ff92b5c5af91fd.jpeg" width="100px;" alt=""/><br /><sub><b>Jeferson Oliveira</b></sub></a><br /></td>
    
  </tr>

</table>


## 📝 Licença

Este projeto esta sobe a licença [MIT](./LICENSE).

Feito com ❤️ por Letícia Vargas e Jeferson Oliveira 👋🏽

---

