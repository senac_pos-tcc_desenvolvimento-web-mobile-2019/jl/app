import { Platform } from 'react-native';
import { Colors } from '@growth/commons/utils';

export default {
  stackScreenOptions: {
    headerStyle: { 
      backgroundColor: Colors.PRIMARY,
      borderWidth: 0,
      elevation: 0,
      shadowOpacity: 0,
    },
    headerTintColor: Colors.WHITE,
    headerTitleStyle: {
      fontWeight: 'bold',
      fontSize: 24,
    },
    title: null,
    headerBackTitle: ' ',
  },

  tabBarOptions: {
    keyboardHidesTabBar: Platform.OS == 'android',
    activeTintColor: Colors.WHITE,
    inactiveTintColor: Colors.WHITE + Colors.OPACITY['50'],
    style: {
      backgroundColor: Colors.PRIMARY,
      borderTopWidth: 0,
      elevation: 0,
      shadowOpacity: 0,
    },
    tabStyle: {
      padding: 4,
      justifyContent: 'space-between',
    },
    iconStyle: {
      marginTop: 4
    },
    labelStyle: {
      fontSize: 12,
    },
  },

  noHeaderOptions: {
    headerShown: false
  }
};