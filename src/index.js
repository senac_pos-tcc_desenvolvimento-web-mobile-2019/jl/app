import React, { useState, useEffect } from 'react';
import { Platform } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NavigationContainer } from '@react-navigation/native';

import { ContextProvider, useContextValue } from '@growth/commons/context';
import { init } from 'locales';

import ScreensNavigator from './screens';


if (Platform.OS == 'android') {
  require('intl');
  require('intl/locale-data/jsonp/pt-BR');
  require('intl/locale-data/jsonp/es');

  if (Intl.__disableRegExpRestore) Intl.__disableRegExpRestore();
}

function App() {
  const contextValue = useContextValue();
  const [initializingLocales, setInitializingLocales] = useState(true);

  useEffect(() => {
    init().then(() => setInitializingLocales(false));
  }, []);

  return (
    <SafeAreaProvider>
      <ContextProvider value={contextValue}>
        <NavigationContainer>
          <ScreensNavigator />
        </NavigationContainer>
      </ContextProvider>
    </SafeAreaProvider>
  );
};

export default App;