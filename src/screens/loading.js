import React from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';

import { Colors } from '@growth/commons/utils';


export default React.memo(({ navigation }) => (
  <View style={styles.container}>
    <ActivityIndicator size="large" color={Colors.WHITE} />
  </View>
));

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY,
  },
});