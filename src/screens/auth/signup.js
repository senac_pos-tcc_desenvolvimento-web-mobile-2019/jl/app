import React, { useState, useMemo, useCallback } from 'react';
import { StyleSheet, View, Alert } from 'react-native';

import { signUp } from '@growth/commons/auth';
import { Colors } from '@growth/commons/utils';
import { ScreenContainer, BrandHeader, TextInput, Checkbox, Button, Text } from 'components';
import { useTranslation } from 'locales';


const SignupScreen = ({ navigation }) => {
  const { t } = useTranslation();

  const [loading, setLoading] = useState();

  const [name, setName] = useState();
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [termsChecked, setTermsChecked] = useState(false);
  const [securePassword, setSecurePassword] = useState(true);

  const [emailHasErrors, setEmailHasErrors] = useState(false);
  const [passwordHasErrors, setPasswordHasErrors] = useState(false);
  const [termsHasErrors, setTermsHasErrors] = useState(false);

  const disableSignup = useMemo(() => {
    const emptyName = !(name && name.trim().length);
    const emptyEmail = !(email && email.trim().length);
    const emptyPassword = !(password && password.trim().length);
    return emptyName || emptyEmail || emptyPassword;
  }, [name, email, password]);

  const passwordRightIcon = useMemo(() => {
    return `eye${securePassword ? '' : '-slash'}`;
  }, [securePassword]);

  const handleNameChange = useCallback((value) => {
    setName(value);
  }, []);

  const handleEmailChange = useCallback((value) => {
    setEmail(value);
  }, []);

  const handlePasswordChange = useCallback((value) => {
    setPassword(value);
  }, []);

  const handlePasswordSecureChange = useCallback(() => {
    setSecurePassword(!securePassword);
  }, [securePassword]);

  const handleTermsCheckChange = useCallback((value) => {
    setTermsChecked(value);
  }, []);

  const handleBack = useCallback(() => {
    navigation.pop();
  }, []);

  const handleSignup = useCallback(() => {
    setLoading(true);
    setEmailHasErrors(false);
    setPasswordHasErrors(false);
    setTermsHasErrors(false);

    if (!termsChecked) {
      setTermsHasErrors(true);
      Alert.alert(
        t('alert.title.error'),
        t('alert.message.auth.error.terms-must-be-accepted'),
        [{ text: t('alert.action.ok'), onPress: setLoading(false) }],
        { cancelable: false }
      );
      return;
    }

    signUp(email, password, name).catch(error => {
      if (error.code.includes('email') || error.code.includes('not-found'))
        setEmailHasErrors(true);
      if (error.code.includes('password'))
        setPasswordHasErrors(true);

      Alert.alert(
        t('alert.title.error'),
        t(error.code.replace('auth/', 'alert.message.auth.error.')),
        [{ text: t('alert.action.ok'), onPress: () => setLoading(false) }],
        { cancelable: false }
      );
    });
  }, [name, email, password, termsChecked]);

  return (
    <ScreenContainer loading={loading} style={styles.screenContainer}>
      <BrandHeader />

      <View style={styles.formContainer}>
        <TextInput
          leftIcon="user"
          leftIconType="regular"
          placeholder={t('models.user.name')}
          value={name}
          onChangeText={handleNameChange}
          style={styles.input}
        />

        <TextInput
          leftIcon="envelope"
          leftIconType="regular"
          keyboardType="email-address"
          autoCapitalize="none"
          hasErrors={emailHasErrors}
          placeholder={t('models.user.email')}
          value={email}
          onChangeText={handleEmailChange}
          style={styles.input}
        />

        <TextInput 
          leftIcon="lock"
          rightIcon={passwordRightIcon}
          rightIconColor={Colors.BLACK_LIGHT}
          onRightIconPress={handlePasswordSecureChange}
          secureTextEntry={securePassword}
          hasErrors={passwordHasErrors}
          placeholder={t('models.user.password')}
          value={password}
          onChangeText={handlePasswordChange}
          style={styles.input}
        />

        <Checkbox
          label={t('screens.auth.signup.input.terms')}
          hasErrors={termsHasErrors}
          value={termsChecked}
          onChange={handleTermsCheckChange}
          style={styles.input}
        />

        <Button
          title={t('screens.auth.signup.action.signup')}
          onPress={handleSignup}
          disabled={disableSignup}
          style={styles.button}
        />

        <Text style={styles.back} onPress={handleBack}>
          {t('screens.auth.signup.action.back')}
        </Text>
      </View>
    </ScreenContainer>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    padding: 0,
  },

  formContainer: {
    ...ScreenContainer.contentContainerStyle,
    paddingHorizontal: 48,
    paddingVertical: 24,
    justifyContent: 'center',
  },

  input: {
    marginBottom: 24,
  },

  button: {
    alignSelf: 'center',
  },

  back: {
    alignSelf: 'center',
    marginTop: 12,
  },
});

export default SignupScreen;