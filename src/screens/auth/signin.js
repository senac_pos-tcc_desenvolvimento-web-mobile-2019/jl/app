import React, { useState, useMemo, useCallback } from 'react';
import { StyleSheet, View, Alert } from 'react-native';

import { signIn } from '@growth/commons/auth';
import { Colors } from '@growth/commons/utils'
import { ScreenContainer, BrandHeader, TextInput, Text, Button } from 'components';
import { useTranslation } from 'locales';


const SigninScreen = ({ navigation }) => {
  const { t } = useTranslation();

  const [loading, setLoading] = useState();

  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [securePassword, setSecurePassword] = useState(true);

  const [emailHasErrors, setEmailHasErrors] = useState(false);
  const [passwordHasErrors, setPasswordHasErrors] = useState(false);

  const disableSignin = useMemo(() => {
    const emptyEmail = !(email && email.trim().length);
    const emptyPassword = !(password && password.trim().length);
    return emptyEmail || emptyPassword;
  }, [email, password]);

  const passwordRightIcon = useMemo(() => {
    return `eye${securePassword ? '' : '-slash'}`;
  }, [securePassword]);

  const handleEmailChange = useCallback((value) => {
    setEmail(value);
  }, []);

  const handlePasswordChange = useCallback((value) => {
    setPassword(value);
  }, []);

  const handlePasswordSecureChange = useCallback(() => {
    setSecurePassword(!securePassword);
  }, [securePassword]);

  const handleRecover = useCallback(() => {
    navigation.navigate('Recover');
  }, []);

  const handleSignup = useCallback(() => {
    navigation.navigate('Signup');
  }, []);

  const handleSignin = useCallback(() => {
    setLoading(true);
    setEmailHasErrors(false);
    setPasswordHasErrors(false);

    signIn(email, password).catch(error => {
      if (error.code.includes('email') || error.code.includes('not-found'))
        setEmailHasErrors(true);
      if (error.code.includes('password'))
        setPasswordHasErrors(true);

      Alert.alert(
        t('alert.title.error'),
        t(error.code.replace('auth/', 'alert.message.auth.error.')),
        [{ text: t('alert.action.ok'), onPress: () => setLoading(false) }],
        { cancelable: false }
      );
    });
  }, [email, password]);

  return (
    <ScreenContainer loading={loading} style={styles.screenContainer}>
      <BrandHeader />

      <View style={styles.formContainer}>
        <TextInput
          leftIcon="envelope"
          leftIconType="regular"
          keyboardType="email-address"
          autoCapitalize="none"
          hasErrors={emailHasErrors}
          placeholder={t('models.user.email')}
          value={email}
          onChangeText={handleEmailChange}
          style={styles.input}
        />

        <TextInput 
          leftIcon="lock"
          rightIcon={passwordRightIcon}
          rightIconColor={Colors.BLACK_LIGHT}
          onRightIconPress={handlePasswordSecureChange}
          secureTextEntry={securePassword}
          hasErrors={passwordHasErrors}
          placeholder={t('models.user.password')}
          value={password}
          onChangeText={handlePasswordChange}
          style={styles.input}
        />

        <Text style={styles.recover} onPress={handleRecover}>
          {t('screens.auth.signin.action.recover')}
        </Text>

        <Button title={t('screens.auth.signin.action.signin')} onPress={handleSignin} disabled={disableSignin} style={styles.button} />

        <Text style={styles.signin} onPress={handleSignup}>
          {t('screens.auth.signin.action.signup.1')} <Text weight="bold">{t('screens.auth.signin.action.signup.2')}</Text>
        </Text>
      </View>
    </ScreenContainer>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    padding: 0,
  },

  formContainer: {
    ...ScreenContainer.contentContainerStyle,
    paddingHorizontal: 48,
    paddingVertical: 24,
    justifyContent: 'center',
  },

  input: {
    marginBottom: 24,
  },

  button: {
    alignSelf: 'center',
  },

  recover: {
    alignSelf: 'flex-end',
    marginTop: -12,
    marginBottom: 32,
  },

  signin: {
    alignSelf: 'center',
    marginTop: 12,
  },
});

export default SigninScreen;