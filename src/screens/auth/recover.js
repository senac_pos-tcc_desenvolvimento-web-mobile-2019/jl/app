import React, { useState, useMemo, useCallback } from 'react';
import { StyleSheet, View, Alert } from 'react-native';

import { resetPassword } from '@growth/commons/auth';
import { ScreenContainer, BrandHeader, Title, Text, TextInput, Button } from 'components';
import { useTranslation, getCurrentLanguage } from 'locales';


const RecoverScreen = ({ navigation }) => {
  const { t } = useTranslation();

  const [loading, setLoading] = useState();
  const [email, setEmail] = useState();
  const [emailHasErrors, setEmailHasErrors] = useState(false);

  const disableRecover = useMemo(() => {
    return !(email && email.trim().length);
  }, [email]);

  const handleEmailChange = useCallback((value) => {
    setEmail(value);
  }, []);

  const handleBack = useCallback(() => {
    navigation.pop();
  }, []);

  const handleRecover = useCallback(() => {
    setLoading(true);
    setEmailHasErrors(false);

    resetPassword(email)
    .then(() => {
      Alert.alert(
        t('alert.title.success'),
        t('alert.message.auth.success.reset-email-sent'),
        [{ text: t('alert.action.ok'), onPress: () => {
          setLoading(false);
          navigation.pop();
        }}],
        { cancelable: false }
      );
    })
    .catch(error => {
      if (error.code.includes('email') || error.code.includes('not-found'))
        setEmailHasErrors(true);

      Alert.alert(
        t('alert.title.error'),
        t(error.code.replace('auth/', 'alert.message.auth.error.')),
        [{ text: t('alert.action.ok'), onPress: () => setLoading(false) }],
        { cancelable: false }
      );
    });
  }, [email]);

  return (
    <ScreenContainer loading={loading} style={styles.screenContainer}>
      <BrandHeader />

      <View style={styles.formContainer}>
        <Title style={styles.spacedElement}>{t('screens.auth.recover.title')}</Title>

        <Text style={styles.spacedElement}>{t('screens.auth.recover.description')}</Text>

        <TextInput
          leftIcon="envelope"
          leftIconType="regular"
          keyboardType="email-address"
          autoCapitalize="none"
          hasErrors={emailHasErrors}
          placeholder={t('models.user.email')}
          value={email}
          onChangeText={handleEmailChange}
          style={styles.spacedElement}
        />

        <Button
          title={t('screens.auth.recover.action.recover')}
          onPress={handleRecover}
          disabled={disableRecover}
          style={styles.button}
        />

        <Text style={styles.back} onPress={handleBack}>
          {t('screens.general.action.back')}
        </Text>
      </View>
    </ScreenContainer>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    padding: 0,
  },

  formContainer: {
    ...ScreenContainer.contentContainerStyle,
    paddingHorizontal: 48,
    paddingVertical: 24,
    justifyContent: 'center',
  },

  spacedElement: {
    marginBottom: 24,
  },

  button: {
    alignSelf: 'center',
  },

  back: {
    alignSelf: 'center',
    marginTop: 12,
  },
});

export default RecoverScreen;