import React, { useMemo } from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { useStatus, useCurrentUser } from '@growth/commons/context';
import { Navigation } from 'utils';

import LoadingScreen from './loading';
import AuthNavigator from './auth';
import MainNavigator from './main';


const Stack = createStackNavigator();

function ScreensNavigator() {
  const status = useStatus();
  const currentUser = useCurrentUser();

  const initializing = useMemo(() => status != 'ready', [status]);
  const authenticated = useMemo(() => status == 'ready' && !!currentUser, [status, currentUser]);

  return (
    <Stack.Navigator screenOptions={Navigation.stackScreenOptions}>
      { initializing && <Stack.Screen name="Loading" component={LoadingScreen} /> }
      { !authenticated && <Stack.Screen name="Auth" options={Navigation.noHeaderOptions} component={AuthNavigator} /> }
      { authenticated && <Stack.Screen name="Main" options={Navigation.noHeaderOptions} component={MainNavigator} /> }
    </Stack.Navigator>
  );
};

export default ScreensNavigator;