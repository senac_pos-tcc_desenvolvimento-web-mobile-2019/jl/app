import React, { useState, useMemo, useCallback } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { StyleSheet } from 'react-native';

import { Colors, Strings } from '@growth/commons/utils';
import { usePublishedTracks, useUserTracks } from '@growth/commons/context';
import { ScreenContainer, TabMenu, Text, TextInput, TracksList } from 'components';
import { useTranslation } from 'locales';


const TABS = ['all', 'my'];

const TracksScreen = ({ route, navigation }) => {
  const { t } = useTranslation();
  const allTracks = usePublishedTracks() || [];
  const userTracks = useUserTracks() || [];

  const [selectedTab, setSelectedTab] = useState('all');
  const [search, setSearch] = useState('');

  useFocusEffect(
    useCallback(() => {
      if (route.params && route.params.tab) {
        setSelectedTab(route.params.tab);
        delete route.params.tab;
      }
    }, [route.params])
  );

  const availableList = useMemo(() => (
    (selectedTab == 'all') ? allTracks : userTracks
  ), [allTracks, userTracks, selectedTab]);

  const searchedList = useMemo(() => (
    availableList.filter(track => Strings.containsSearch(track.name, search))
  ), [availableList, search]);

  const handleSearchChange = useCallback((value) => {
    setSearch(value)
  }, []);

  const handleTabPress = useCallback((tab) => {
    setSelectedTab(tab);
  }, []);

  const handleTrackPress = useCallback((track) => {
    navigation.navigate('Track', { id: track.id });
  }, []);

  const getTabLabel = useCallback((tab) => {
    return t('models.track.' + tab);
  }, [selectedTab]);

  const isTabSelected = useCallback((tab) => {
    return tab == selectedTab;
  }, [selectedTab]);

  const renderHeader = useCallback(() => (
    <TabMenu
      data={TABS}
      labelExtractor={getTabLabel}
      onPress={handleTabPress}
      isSelected={isTabSelected}
    />
  ), [getTabLabel, handleTabPress, isTabSelected]);

  return (
    <ScreenContainer renderHeader={renderHeader} stickyHeaderIndices={[1]} >
      <Text style={styles.text}>{t('screens.main.tabs.tracks.description.' + selectedTab)}</Text>

      <TextInput
        rightIcon="search"
        autoCapitalize="none"
        placeholder={t('screens.main.tabs.tracks.search')}
        value={search}
        onChangeText={handleSearchChange}
        style={styles.search}
      />

      <TracksList tracks={searchedList} onPress={handleTrackPress} />
    </ScreenContainer>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    padding: 0,
  },

  text: {
    paddingBottom: 12,
  },

  search: {
    backgroundColor: Colors.BACKGROUND,
    paddingTop: 12,
    paddingBottom: 24,
  }
});


export default TracksScreen;