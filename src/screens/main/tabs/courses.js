import React, { useState, useMemo, useCallback } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { StyleSheet } from 'react-native';

import { Colors, Strings } from '@growth/commons/utils';
import { usePublishedCourses, useUserCourses } from '@growth/commons/context';
import { ScreenContainer, TabMenu, Text, TextInput, CoursesList } from 'components';
import { useTranslation } from 'locales';


const TABS = ['all', 'my'];

const CoursesScreen = ({ route, navigation }) => {
  const { t } = useTranslation();
  const allCourses = usePublishedCourses() || [];
  const userCourses = useUserCourses() || [];

  const [selectedTab, setSelectedTab] = useState('all');
  const [search, setSearch] = useState('');

  useFocusEffect(
    useCallback(() => {
      if (route.params && route.params.tab) {
        setSelectedTab(route.params.tab);
        delete route.params.tab;
      }
    }, [route.params])
  );

  const availableList = useMemo(() => (
    (selectedTab == 'all') ? allCourses : userCourses
  ), [allCourses, userCourses, selectedTab]);

  const searchedList = useMemo(() => (
    availableList.filter(course => Strings.containsSearch(course.name, search))
  ), [availableList, search]);

  const handleSearchChange = useCallback((value) => {
    setSearch(value)
  }, []);

  const handleTabPress = useCallback((tab) => {
    setSelectedTab(tab);
  }, []);

  const handleCoursePress = useCallback((course) => {
    navigation.navigate('Course', { id: course.id });
  }, []);

  const getTabLabel = useCallback((tab) => {
    return t('models.course.' + tab);
  }, [selectedTab]);

  const isTabSelected = useCallback((tab) => {
    return tab == selectedTab;
  }, [selectedTab]);

  const renderHeader = useCallback(() => (
    <TabMenu
      data={TABS}
      labelExtractor={getTabLabel}
      onPress={handleTabPress}
      isSelected={isTabSelected}
    />
  ), [getTabLabel, handleTabPress, isTabSelected]);

  return (
    <ScreenContainer renderHeader={renderHeader} stickyHeaderIndices={[1]} >
      <Text style={styles.text}>{t('screens.main.tabs.courses.description.' + selectedTab)}</Text>

      <TextInput
        rightIcon="search"
        autoCapitalize="none"
        placeholder={t('screens.main.tabs.courses.search')}
        value={search}
        onChangeText={handleSearchChange}
        style={styles.search}
      />

      <CoursesList courses={searchedList} onPress={handleCoursePress} />
    </ScreenContainer>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    padding: 0,
  },

  text: {
    paddingBottom: 12,
  },

  search: {
    backgroundColor: Colors.BACKGROUND,
    paddingTop: 12,
    paddingBottom: 24,
  }
});


export default CoursesScreen;