import React, { useCallback } from 'react';
import { StyleSheet, View } from 'react-native';

import { Colors } from '@growth/commons/utils';
import { signOut } from '@growth/commons/auth';
import { useCurrentUser, useUserCourses, useUserCoursesDone, useUserTracks, useUserTracksDone } from '@growth/commons/context';
import { ScreenContainer, ScreenHeader, UserAvatar, Text, CoursesList, TracksList } from 'components';
import { useTranslation } from 'locales';


const DashboardScreen = ({ navigation }) => {
  const { t } = useTranslation();
  const courses = useUserCourses(courses => courses.slice(0, 2));
  const tracks = useUserTracks(tracks => tracks.slice(0, 2));

  const [loading, setLoading] = React.useState(false);

  const handleCoursePress = useCallback((course) => {
    navigation.navigate('Course', { id: course.id });
  }, []);

  const handleMoreCourses = useCallback(() => {
    navigation.navigate('Courses', { tab: 'my' });
  }, []);

  const handleTrackPress = useCallback((track) => {
    navigation.navigate('Track', { id: track.id });
  }, []);

  const handleMoreTracks = useCallback(() => {
    navigation.navigate('Tracks', { tab: 'my' });
  }, []);

  const renderHeader = useCallback(() => (
    <DashboardHeader />
  ), []);

  return (
    <ScreenContainer renderHeader={renderHeader}>
      <CoursesList title={t('models.course.my')} courses={courses} onPress={handleCoursePress} />
      <Text onPress={handleMoreCourses} style={styles.more}>{t('screens.general.action.more')}</Text>

      <TracksList title={t('models.track.my')} tracks={tracks} onPress={handleTrackPress} style={styles.tracksList} />
      <Text onPress={handleMoreTracks} style={styles.more}>{t('screens.general.action.more')}</Text>
    </ScreenContainer>
  );
};


const DashboardHeader = () => {
  const { t } = useTranslation();
  const user = useCurrentUser();
  const coursesDone = useUserCoursesDone();
  const tracksDone = useUserTracksDone();

  return (
    <ScreenHeader
      avatarUri={user && user.avatarUrl}
      avatarString={user && user.name}
      title={user && user.name}
      subtitle={user && user.email}
      contentContainerStyle={styles.headerContent}
    >
      <ScreenHeader.Info
        text={`${t('models.course.all')}\n${t('models.course.status.done', { count: 2 })}`}
        subtitle={coursesDone.length}
      />
      <ScreenHeader.Info
        text={`${t('models.track.all')}\n${t('models.track.status.done', { count: 2 })}`}
        subtitle={tracksDone.length}
      />
    </ScreenHeader>
  );
};

const styles = StyleSheet.create({
  more: {
    alignSelf: 'flex-end',
    color: Colors.PRIMARY,
    marginVertical: 8,
  },

  tracksList: {
    marginTop: 12,
  },

  headerContent: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
});

export default DashboardScreen;