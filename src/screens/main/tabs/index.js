import React, { useLayoutEffect } from 'react';
import { StyleSheet } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { Colors } from '@growth/commons/utils';
import { Navigation } from 'utils';
import { Icon } from 'components';
import { useTranslation } from 'locales';

import DashboardScreen from './dashboard';
import TracksScreen from './tracks';
import CoursesScreen from './courses';


const Tab = createBottomTabNavigator();

const TabsNavigator = ({ navigation }) => {
  const { t } = useTranslation();

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Icon
          name="cog"
          type="solid"
          size={20}
          color={Colors.WHITE}
          style={styles.headerRight}
          onPress={() => navigation.navigate('Settings')}
        />
      ),
    });
  }, [navigation]);

  return (
    <Tab.Navigator 
      tabBarOptions={Navigation.tabBarOptions}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color }) => getTabIcon({ route, color })
      })}
    >
      <Tab.Screen name="Dashboard" component={DashboardScreen} options={{ tabBarLabel: t('screens.main.tabs.dashboard.title') }} />
      <Tab.Screen name="Tracks" component={TracksScreen} options={{ tabBarLabel: t('screens.main.tabs.tracks.title') }} />
      <Tab.Screen name="Courses" component={CoursesScreen} options={{ tabBarLabel: t('screens.main.tabs.courses.title') }} />
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  headerRight: {
    marginRight: 8,
    padding: 8,
  }
});

const getTabIcon = ({ route, color }) => {
  const iconProps = {
    name: route.name == 'Tracks' ? 'buffer' : route.name == 'Dashboard' ? 'home' : 'book-open',
    type: route.name == 'Tracks' ? 'brands' : 'solid',
    size: 20,
    color,
  };

  return <Icon { ...iconProps } />;
};

export default TabsNavigator;