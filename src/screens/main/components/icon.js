import React from 'react';
import { StyleSheet, View } from 'react-native';

import { Colors } from '@growth/commons/utils';
import { ScreenContainer, Subtitle, Text, Separator, Icon } from 'components';
import { useTranslation } from 'locales';


const IconShowcaseScreen = () => {
  const { t } = useTranslation();

  return (
    <ScreenContainer>
      <Text style={styles.text}>{t('components.icon.description')}</Text>
      <Text style={styles.text}>
        <Text weight="bold">{t('components.icon.note.title')}</Text>
        <Text>{t('components.icon.note.text')}</Text>
      </Text>
      <Separator />

      <View style={styles.row}>
        <Icon name="address-book" size={48} style={styles.icon} />
        <Icon name="arrow-alt-circle-up" size={48} style={styles.icon} />
        <Icon name="calendar" size={48} style={styles.icon} />
        <Icon name="comment-alt" size={48} style={styles.icon} />
      </View>

      <View style={styles.row}>
        <Icon name="address-book" type="regular" size={48} style={styles.icon} />
        <Icon name="arrow-alt-circle-up" type="regular" size={48} style={styles.icon} />
        <Icon name="calendar" type="regular" size={48} style={styles.icon} />
        <Icon name="comment-alt" type="regular" size={48} style={styles.icon} />
      </View>

      <View style={styles.row}>
        <Icon name="user" size={32} color={Colors.BLACK_LIGHT} style={styles.icon} />
        <Icon name="caret-square-down" size={32} color={Colors.BLACK_LIGHT} style={styles.icon} />
        <Icon name="edit" size={32} color={Colors.BLACK_LIGHT} style={styles.icon} />
        <Icon name="envelope" size={32} color={Colors.BLACK_LIGHT} style={styles.icon} />
      </View>

      <View style={styles.row}>
        <Icon name="user" type="regular" size={32} color={Colors.BLACK_LIGHT} style={styles.icon} />
        <Icon name="caret-square-down" type="regular" size={32} color={Colors.BLACK_LIGHT} style={styles.icon} />
        <Icon name="edit" type="regular" size={32} color={Colors.BLACK_LIGHT} style={styles.icon} />
        <Icon name="envelope" type="regular" size={32} color={Colors.BLACK_LIGHT} style={styles.icon} />
      </View>

      <View style={styles.row}>
        <Icon name="facebook" type="brands" size={54} color="#2D88FF" style={styles.icon} />
        <Icon name="adobe" type="brands" size={54} color="#FA0F00" style={styles.icon} />
        <Icon name="gitlab" type="brands" size={54} color="#FCA326" style={styles.icon} />
      </View>
    </ScreenContainer>
  );
};

const styles = StyleSheet.create({
  text: {
    marginBottom: 8,
  },

  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 8,
  },

  icon: {
    margin: 4,
  }
});

export default IconShowcaseScreen;