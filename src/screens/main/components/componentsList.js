import React, { useMemo, useCallback } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';

import { ScreenContainer, Separator, Subtitle, Text, Icon } from 'components';
import { useTranslation } from 'locales';


const ComponentsList = ({ navigation }) => {
  const { t } = useTranslation();

  return (
    <ScreenContainer>
      <Subtitle style={styles.subtitle}>{t('components.sections.textsAndIcons')}</Subtitle>
      <Separator style={styles.separator} />
      <ListItem title="Text" onPress={() => navigation.navigate('TextShowcase')} />
      <ListItem title="Icon" onPress={() => navigation.navigate('IconShowcase')} />

      <Subtitle style={styles.subtitle}>{t('components.sections.buttons')}</Subtitle>
      <Separator style={styles.separator} />
      <ListItem title="Button" onPress={() => navigation.navigate('ButtonShowcase')} />

      <Subtitle style={styles.subtitle}>{t('components.sections.inputs')}</Subtitle>
      <Separator style={styles.separator} />
      <ListItem title="TextInput" onPress={() => navigation.navigate('TextInputShowcase')} />
      <ListItem title="Checkbox" onPress={() => navigation.navigate('CheckboxShowcase')} />
    </ScreenContainer>
  );
};

const ListItem = React.memo(({ title, style, ...otherProps }) => {
  const containerStyle = useMemo(() => ({ ...styles.listItemContainer, ...style }), [style]);

  return (
    <React.Fragment>
      <TouchableOpacity style={containerStyle} { ...otherProps }>
        <Text>{title}</Text>
        <Icon name="chevron-right" size={14} />
      </TouchableOpacity>
      <Separator style={styles.separator} />
    </React.Fragment>
  );
});

const styles = StyleSheet.create({
  subtitle: {
    paddingTop: 16,
    paddingBottom: 4,
  },

  separator: {
    marginVertical: 0,
  },

  listItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 12,
  }
});

export default ComponentsList;