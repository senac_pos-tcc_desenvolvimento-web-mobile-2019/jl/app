import React, { useCallback } from 'react';
import { StyleSheet, Alert } from 'react-native';

import { ScreenContainer, Text, Separator, Button } from 'components';
import { useTranslation } from 'locales';


const ButtonShowcaseScreen = () => {
  const { t } = useTranslation();

  const handleButtonPress = useCallback(() => Alert.alert(
    null,
    t('components.button.alert'),
    [
      { text: t('alert.action.ok'), onPress: () => {} }
    ],
    { cancelable: false }
  ), []);

  return (
    <ScreenContainer>
      <Text style={styles.text}>{t('components.button.description')}</Text>
      <Separator />

      <Button
        title={t('components.button.simple')}
        style={styles.button}
        onPress={handleButtonPress}
      />
      <Button
        title={t('components.button.disabled')}
        style={styles.button}
        disabled={true}
      />
      <Button
        title={t('components.button.leftIcon')}
        leftIcon="chevron-left"
        style={styles.button}
        onPress={handleButtonPress}
      />
      <Button
        title={t('components.button.rightIcon')}
        rightIcon="chevron-right"
        style={styles.button}
        onPress={handleButtonPress}
      />
      <Button
        rightIcon="check-square"
        style={styles.button}
        onPress={handleButtonPress}
      />
      <Button
        title={t('components.button.small')}
        style={styles.smallButton}
        onPress={handleButtonPress}
      />
    </ScreenContainer>
  );
};

const styles = StyleSheet.create({
  text: {
    marginBottom: 8,
  },

  button: {
    marginBottom: 8,
    alignSelf: 'center',
  },

  smallButton: {
    marginBottom: 8,
    alignSelf: 'center',
    width: 'auto',
  }
});

export default ButtonShowcaseScreen;