import React, { useCallback } from 'react';
import { StyleSheet, Alert } from 'react-native';

import { Colors } from '@growth/commons/utils';
import { ScreenContainer, Title, Subtitle, Text, Separator } from 'components';
import { useTranslation } from 'locales';


const TextShowcaseScreen = () => {
  const { t } = useTranslation();

  const handleTextPress = useCallback(() => Alert.alert(
    null,
    t('components.text.touchableTextAlert'),
    [
      { text: t('alert.action.ok'), onPress: () => {} }
    ],
    { cancelable: false }
  ), []);

  return (
    <ScreenContainer>
      <Text style={styles.text}>{t('components.text.description')}</Text>
      <Separator />

      <Title style={styles.text}>
        {t('components.text.title')}
      </Title>

      <Subtitle style={styles.text}>
        {t('components.text.subtitle')}
      </Subtitle>

      <Text style={styles.text}>
        {t('components.text.plainText')}
      </Text>

      <Text style={styles.text} onPress={handleTextPress}>
        {t('components.text.touchableText')}
      </Text>

      <Text style={styles.text}>
        {t('components.text.nestedText.part1')}
        <Text weight="bold">
          {t('components.text.nestedText.part2')}
          <Text weight="bold" style={styles.nestedText}>
            {t('components.text.nestedText.part3')}
          </Text>
        </Text>
      </Text>
    </ScreenContainer>
  );
};

const styles = StyleSheet.create({
  text: {
    marginBottom: 16,
  },

  nestedText: {
    color: Colors.PRIMARY,
    marginBottom: 16,
  }
});

export default TextShowcaseScreen;