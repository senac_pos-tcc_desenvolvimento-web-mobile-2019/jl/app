import React, { useState, useCallback } from 'react';
import { StyleSheet } from 'react-native';

import { ScreenContainer, Text, Separator, Checkbox } from 'components';
import { useTranslation } from 'locales';


const CheckboxShowcaseScreen = () => {
  const { t } = useTranslation();

  const [checked, setChecked] = useState(false);

  const handleCheck = useCallback((value) => setChecked(value), []);

  return (
    <ScreenContainer>
      <Text style={styles.text}>{t('components.checkbox.description')}</Text>
      <Separator />

      <Checkbox
        label={t('components.checkbox.simple')}
        value={checked}
        style={styles.checkbox}
        onChange={handleCheck}
      />
      <Checkbox
        label={t('components.checkbox.multiline')}
        value={checked}
        onChange={handleCheck}
        style={styles.checkbox}
      />
      <Checkbox
        label={t('components.checkbox.disabled')}
        value={checked}
        style={styles.checkbox}
        disabled={true}
      />
      <Checkbox
        label={t('components.checkbox.hasErrors')}
        value={checked}
        style={styles.checkbox}
        onChange={handleCheck}
        hasErrors={true}
      />
    </ScreenContainer>
  );
};

const styles = StyleSheet.create({
  text: {
    marginBottom: 8,
  },

  checkbox: {
    marginBottom: 8,
    alignSelf: 'center',
  },
});

export default CheckboxShowcaseScreen;