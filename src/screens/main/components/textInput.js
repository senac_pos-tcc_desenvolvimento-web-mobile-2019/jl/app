import React, { useCallback } from 'react';
import { StyleSheet, Alert } from 'react-native';

import { ScreenContainer, Text, Separator, TextInput } from 'components';
import { useTranslation } from 'locales';


const TextInputShowcaseScreen = () => {
  const { t } = useTranslation();

  const handleLeftIconPress = useCallback(() => Alert.alert(
    null,
    t('components.textInput.alert.leftIcon'),
    [
      { text: t('alert.action.ok'), onPress: () => {} }
    ],
    { cancelable: false }
  ), []);

  const handleRightIconPress = useCallback(() => Alert.alert(
    null,
    t('components.textInput.alert.rightIcon'),
    [
      { text: t('alert.action.ok'), onPress: () => {} }
    ],
    { cancelable: false }
  ), []);

  return (
    <ScreenContainer>
      <Text style={styles.text}>{t('components.textInput.description')}</Text>
      <Separator />

      <TextInput
        placeholder={t('components.textInput.simple')}
        style={styles.textInput}
      />
      <TextInput
        placeholder={t('components.textInput.numeric')}
        keyboardType="numeric"
        style={styles.textInput}
      />
      <TextInput
        placeholder={t('components.textInput.email')}
        keyboardType="email-address"
        style={styles.textInput}
      />
      <TextInput
        placeholder={t('components.textInput.leftIcon')}
        leftIcon="envelope"
        style={styles.textInput}
      />
      <TextInput
        placeholder={t('components.textInput.rightIcon')}
        rightIcon="calendar"
        style={styles.textInput}
      />
      <TextInput
        placeholder={t('components.textInput.bothIcons')}
        leftIcon="caret-square-left"
        rightIcon="caret-square-right"
        style={styles.textInput}
      />
      <TextInput
        placeholder={t('components.textInput.customIcons')}
        leftIcon="chevron-circle-left"
        leftIconColor="purple"
        rightIcon="chevron-circle-right"
        rightIconColor="brown"
        style={styles.textInput}
      />
      <TextInput
        placeholder={t('components.textInput.pressableIcons')}
        leftIcon="hand-point-left"
        onLeftIconPress={handleLeftIconPress}
        rightIcon="hand-point-right"
        onRightIconPress={handleRightIconPress}
        style={styles.textInput}
      />

      <TextInput
        placeholder={t('components.textInput.hasErrors')}
        leftIcon="exclamation-triangle"
        hasErrors={true}
        style={styles.textInput}
      />
      <TextInput
        value={t('components.textInput.notEditable')}
        leftIcon="ban"
        editable={false}
        style={styles.textInput}
      />
      <TextInput
        value={t('components.textInput.disabled')}
        leftIcon="ban"
        disabled={true}
        style={styles.textInput}
      />
    </ScreenContainer>
  );
};

const styles = StyleSheet.create({
  text: {
    marginBottom: 8,
  },

  textInput: {
    marginVertical: 12,
  }
});

export default TextInputShowcaseScreen;