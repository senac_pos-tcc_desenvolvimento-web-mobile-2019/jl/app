import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { Navigation } from 'utils';

import ComponentsListScreen from './componentsList';
import TextShowcaseScreen from './text';
import IconShowcaseScreen from './icon';
import ButtonShowcaseScreen from './button';
import TextInputShowcaseScreen from './textInput';
import CheckboxShowcaseScreen from './checkbox';


const Stack = createStackNavigator();

function ComponentsNavigator() {
  return (
    <Stack.Navigator screenOptions={Navigation.stackScreenOptions}>
      <Stack.Screen name="ComponentsList" options={{ title: "Components" }} component={ComponentsListScreen} />
      <Stack.Screen name="TextShowcase" options={{ title: "Text" }} component={TextShowcaseScreen} />
      <Stack.Screen name="IconShowcase" options={{ title: "Icon" }} component={IconShowcaseScreen} />
      <Stack.Screen name="ButtonShowcase" options={{ title: "Button" }} component={ButtonShowcaseScreen} />
      <Stack.Screen name="TextInputShowcase" options={{ title: "TextInput" }} component={TextInputShowcaseScreen} />
      <Stack.Screen name="CheckboxShowcase" options={{ title: "Checkbox" }} component={CheckboxShowcaseScreen} />
    </Stack.Navigator>
  );
};

export default ComponentsNavigator;