import React, { useState, useMemo, useEffect, useLayoutEffect, useCallback } from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { WebView } from 'react-native-webview';

import { Colors, Constants } from '@growth/commons/utils';
import { useCurrentUser, usePublishedCourses, useUserCourseProgress } from '@growth/commons/context';
import { ScreenContainer, ScreenHeader, Icon , Text, Button, Modal } from 'components';
import { useTranslation, getCurrentLanguage } from 'locales';


const CourseScreen = ({ route, navigation }) => {
  const { t } = useTranslation();
  const user = useCurrentUser();
  const course = usePublishedCourses(courses => courses.find(course => course.id == route.params.id));
  const { status, accessedAt, startedAt, doneAt, ratedAs } = useUserCourseProgress(course);

  const [loading, setLoading] = useState(false);
  const [ratingVisible, setRatingVisible] = React.useState(false);
  const [rating, setRating] = useState(ratedAs);

  const mediaSource = useMemo(() => ({
    uri: course.mediaType == 'video' ? course.videoUrl : course.imageUrl
  }), [course]);

  const buttonProps = useMemo(() => ({
    title: t(`screens.main.course.action.${!status ? 'start' : status == Constants.PROGRESS_STATUS.PENDING ? 'finish' : 'rate' }`),
    rightIcon: !status ? 'play' : status == Constants.PROGRESS_STATUS.PENDING ? 'check-circle' : 'star'
  }), [status]);

  useEffect(() => {
    if (course)
      user.setCourseAccess(course.id);
  }, []);

  useEffect(() => {
    if (!course)
      navigation.goBack();
  }, [course]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: (status == Constants.PROGRESS_STATUS.DONE) ? () => (
        <Icon
          name="trash-alt"
          type="solid"
          size={20}
          color={Colors.WHITE}
          style={styles.headerRight}
          onPress={() => {
            setLoading(true);
            user.deleteCourseProgress(course.id);
            setTimeout(() => setLoading(false), 750);
          }}
        />
      ) : null,
    });
  }, [user, status, navigation]);

  const handleButonPress = useCallback(() => {
    if (status == Constants.PROGRESS_STATUS.DONE) {
      setRatingVisible(true);
    } else {
      setLoading(true);

      if (!status)
        user.setCourseProgress(course.id, Constants.PROGRESS_STATUS.PENDING);
      else if (status == Constants.PROGRESS_STATUS.PENDING)
        user.setCourseProgress(course.id, Constants.PROGRESS_STATUS.DONE);

      setTimeout(() => {
        setLoading(false);
        if (status == Constants.PROGRESS_STATUS.PENDING)
          setRatingVisible(true);
      }, 750);
    }
  }, [status, user]);

  const handleRatingPress = useCallback((value) => {
    setRating(value);
  }, []);

  const handleRatingCancel = useCallback(() => {
    setRatingVisible(false);
  }, []);

  const handleRatingConfirm = useCallback(() => {
    user.setCourseRating(course.id, rating);
    setRatingVisible(false);
  }, [rating]);

  const renderHeader = useCallback(() => (
    <CourseScreenHeader course={course} />
  ), [course]);

  return (
    <ScreenContainer loading={loading} renderHeader={renderHeader}>
      {(status != Constants.PROGRESS_STATUS.DONE || !ratedAs) && (
        <Button
          { ...buttonProps }
          style={styles.button}
          iconSize={14}
          onPress={handleButonPress}
        />
      )}

      {status == Constants.PROGRESS_STATUS.DONE && !!ratedAs && (
        <CourseRating rating={rating} size={18} style={styles.button} />
      )}

      {!!course && course.mediaType == 'image' && (
        <Image
          source={mediaSource}
          style={styles.media}
          resizeMode="contain"
        />
      )}
      {!!course && course.mediaType == 'video' && (
        <WebView
          source={mediaSource}
          containerStyle={styles.media}
          allowsFullscreenVideo={true}
          useWebKit={true}
          allowsInlineMediaPlayback={true}
          mediaPlaybackRequiresUserAction={true}
          javaScriptEnabled={true}
          scrollEnabled={false}
        />
      )}
      <Text>{course && course.description}</Text>

      <Modal
        visible={ratingVisible}
        title={t('screens.main.course.alert.rateTitle')}
        confirmTitle={t('alert.action.send')}
        confirmDisabled={!rating}
        onConfirm={handleRatingConfirm}
        cancelTitle={t('alert.action.notNow')}
        onCancel={handleRatingCancel}
      >
        <Text style={{ textAlign: 'center' }}>{t('screens.main.course.alert.rateText')}</Text>
        <Text style={{ textAlign: 'center', color: Colors.BLACK_LIGHT }}>{t('screens.main.course.alert.rateTip')}</Text>
        <CourseRating rating={rating} onPress={handleRatingPress} />
      </Modal>
    </ScreenContainer>
  );
};

const CourseRating = ({ rating, size = 32, onPress, style }) => (
  <View style={{ ...styles.ratingContainer, ...style }}>
    {[...Array(5)].map((_, index) => (
      <Icon
        key={index}
        color={Colors.GOLD}
        size={size}
        name="star"
        type={rating >= (index + 1) ? 'solid' : 'regular'}
        style={index && styles.ratingStar}
        onPress={onPress ? () => onPress(index + 1) : undefined}
      />
    ))}
  </View>
);


const CourseScreenHeader = React.memo(({ course }) => {
  const { t } = useTranslation();
  const { status, startedAt, doneAt } = useUserCourseProgress(course);

  const accessInfo = useMemo(() => {
    if (status == Constants.PROGRESS_STATUS.PENDING)
      return !!startedAt && `${t('models.course.startedAt')} ${startedAt.toLocaleDateString(getCurrentLanguage())}`;
    if (status == Constants.PROGRESS_STATUS.DONE)
      return !!doneAt && `${t('models.course.doneAt')} ${doneAt.toLocaleDateString(getCurrentLanguage())}`;
  }, [status, startedAt, doneAt ]);

  return (
    <ScreenHeader
      avatarUri={course.badgeUrl}
      title={course && course.name}
      titleNumberOfLines={2}
      subtitle={accessInfo}
      contentContainerStyle={styles.headerContent}
    >
      <ScreenHeader.Info
        iconName="clock"
        text={t('models.course.estimatedTimeCount', { count: course ? course.estimatedTime : 0 })}
      />

      <ScreenHeader.Info
        iconName="star-half-alt"
        iconType="solid"
        text={course.rating ? parseFloat((course.rating.sum/course.rating.count).toFixed(1)).toLocaleString() : '-'}
      />
    </ScreenHeader>
  );
});

const styles = StyleSheet.create({
  headerRight: {
    marginRight: 8,
    padding: 8,
  },

  button: {
    alignSelf: 'flex-end',
    width: 'auto',
    marginBottom: 12
  },

  media: {
    flex: 0,
    width: "100%",
    height: 180,
    marginBottom: 12
  },

  headerContent: {
    flexDirection: 'row',
    justifyContent: 'space-evenly'
  },

  ratingContainer: {
    flexDirection: 'row',
    marginTop: 4
  },

  ratingStar: {
    marginLeft: 4,
  },
});

export default CourseScreen;