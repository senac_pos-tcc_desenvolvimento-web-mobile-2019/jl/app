import React, { useState, useMemo, useLayoutEffect, useCallback } from 'react';
import { StyleSheet, View, Alert } from 'react-native';

import { Colors, Constants } from '@growth/commons/utils';
import { signOut } from '@growth/commons/auth';
import { useCurrentUser } from '@growth/commons/context';
import { ScreenContainer, UserAvatar, TextInput, Checkbox, Button, LanguageButton, Text, Icon } from 'components';
import { useTranslation, getCurrentLanguage } from 'locales';


const DashboardScreen = ({ navigation }) => {
  const { t } = useTranslation();
  const user = useCurrentUser();

  const [loading, setLoading] = useState(false);

  const [name, setName] = useState(user && user.name);
  const [phone, setPhone] = useState(user && user.phone);
  const [birthday, setBirthday] = useState(user && user.birthday.slice(0, BIRTHDAY_MAX_LENGTH));
  const [city, setCity] = useState(user && user.city);

  const disableSave = useMemo(() => {
    return !(name && name.trim().length);
  }, [name]);

  useLayoutEffect(() => {
    if (user && user.role == Constants.ROLE.ADMIN)
      navigation.setOptions({
        headerRight: () => (
          <Icon
            name="code"
            type="solid"
            size={20}
            color={Colors.WHITE}
            style={styles.headerRight}
            onPress={() => navigation.navigate('Components')}
          />
        ),
      });
  }, [user && user.role, navigation]);

  const handleNameChange = useCallback((value) => {
    setName(value);
  }, []);

  const handlePhoneChange = useCallback((value) => {
    setPhone(value);
  }, []);

  const handleBirthdayChange = useCallback((value) => {
    setBirthday(value);
  }, []);

  const handleCityChange = useCallback((value) => {
    setCity(value);
  }, []);

  const handleSave = useCallback(() => {
    setLoading(true);
    user.update({ name, phone, birthday, city });
    setTimeout(() => setLoading(false), 1000);
  }, [name, phone, birthday, city]);

  const handleSignOut = useCallback(() => {
    setLoading(true);
    signOut().then(() => setLoading(false));
  }, []);

  return (
    <ScreenContainer loading={loading}>
      <UserAvatar size={120} style={styles.avatar} />

      {!!user && user.accessedAt && (
        <Text style={styles.access}>
          {t('models.user.accessedAt')}: {user.accessedAt.toLocaleString(getCurrentLanguage())}
        </Text>
      )}

      <View style={styles.formContainer}>
        <TextInput
          leftIcon="envelope"
          leftIconType="regular"
          disabled={true}
          value={user && user.email}
          style={styles.input}
        />

        <TextInput
          leftIcon="user"
          leftIconType="regular"
          placeholder={t('models.user.name')}
          value={name}
          onChangeText={handleNameChange}
          style={styles.input}
        />

        <TextInput
          leftIcon="phone"
          leftIconType="solid"
          keyboardType="numeric"
          placeholder={t('models.user.phone')}
          value={phone}
          onChangeText={handlePhoneChange}
          style={styles.input}
        />

        <TextInput
          leftIcon="birthday-cake"
          leftIconType="solid"
          keyboardType="numeric"
          placeholder={t('models.user.birthday')}
          maxLength={BIRTHDAY_MAX_LENGTH}
          value={birthday}
          onChangeText={handleBirthdayChange}
          style={styles.input}
        />

        <TextInput
          leftIcon="map-marker-alt"
          leftIconType="solid"
          placeholder={t('models.user.city')}
          value={city}
          onChangeText={handleCityChange}
          style={styles.input}
        />

        <LanguageButton style={styles.input} />
      </View>

      <Button
        title={t('screens.general.action.save')}
        onPress={handleSave}
        disabled={disableSave}
        style={styles.button}
      />

      <Text style={styles.back} onPress={handleSignOut}>
        {t('screens.main.settings.action.signOut')}
      </Text>
    </ScreenContainer>
  );
};

const BIRTHDAY_MAX_LENGTH = 10;

const styles = StyleSheet.create({
  headerRight: {
    marginRight: 8,
    padding: 8,
  },

  avatar: {
    alignSelf: 'center',
    marginBottom: 48,
  },

  access: {
    fontSize: 12,
    color: Colors.BLACK_LIGHT,
    alignSelf: 'center',
    marginBottom: 12,
  },

  formContainer: {
    flex: 1,
    paddingHorizontal: 24,
  },

  input: {
    marginBottom: 24,
  },

  button: {
    alignSelf: 'center',
  },

  back: {
    alignSelf: 'center',
    marginTop: 12,
  },
});

export default DashboardScreen;