import React, { useEffect, useCallback, useMemo } from 'react';
import { StyleSheet, View } from 'react-native';

import { Colors, Constants } from '@growth/commons/utils';
import { usePublishedTracks, useTrackCourses, useTrackCoursesInfo, useUserTrackProgress } from '@growth/commons/context';
import { ScreenContainer, ScreenHeader, Text, ProgressBar, CoursesList } from 'components';
import { useTranslation, getCurrentLanguage } from 'locales';


const TrackScreen = ({ route, navigation }) => {
  const track = usePublishedTracks(tracks => tracks.find(track => track.id == route.params.id));
  const courses = useTrackCourses(track);

  useEffect(() => {
    if (!track)
      navigation.goBack();
  }, [track]);

  const handleCoursePress = useCallback((course) => {
    navigation.navigate('Course', { id: course.id });
  }, []);

  const renderHeader = useCallback(() => (
    <TrackScreenHeader track={track} />
  ), [track]);

  return (
    <ScreenContainer renderHeader={renderHeader}>
      <Text style={styles.description}>{track && track.description}</Text>
      <CoursesList courses={courses} onPress={handleCoursePress} />
    </ScreenContainer>
  );
};


const TrackScreenHeader = ({ track }) => {
  const { t } = useTranslation();
  const { courses, estimatedTime } = useTrackCoursesInfo(track);
  const { status, startedAt, doneAt, done, percentage } = useUserTrackProgress(track);

  const accessInfo = useMemo(() => {
    if (status == Constants.PROGRESS_STATUS.PENDING)
      return !!startedAt && `${t('models.course.startedAt')} ${startedAt.toLocaleDateString(getCurrentLanguage())}`;
    if (status == Constants.PROGRESS_STATUS.DONE)
      return !!doneAt && `${t('models.course.doneAt')} ${doneAt.toLocaleDateString(getCurrentLanguage())}`;
  }, [status, startedAt, doneAt ]);

  return (
    <ScreenHeader
      avatarUri={track.badgeUrl}
      title={track && track.name}
      titleNumberOfLines={2}
      subtitle={accessInfo}
      contentContainerStyle={styles.headerContent}
    >
      <ScreenHeader.Info
        iconName="clock"
        text={t('models.track.estimatedTimeCount', { count: estimatedTime })}
      />
      <View style={styles.headerProgressContainer}>
        <ProgressBar style={styles.headerProgressBar} color={Colors.WHITE} labelColor={Colors.PRIMARY} progressPercentage={percentage} />
        <View style={styles.headerProgressTextContainer}>
          <Text style={styles.headerProgressCount}>{done.length}/{courses.length}</Text>
          <Text style={styles.headerProgressText}>
            {`${t('models.course.all')}\n${t('models.course.status.done', { count: 2 })}`}
          </Text>
        </View>
      </View>
    </ScreenHeader>
  );
};

const styles = StyleSheet.create({
  description: {
    marginBottom: 24
  },

  headerContent: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },

  headerProgressContainer: {
    alignItems: 'center',
    flex: 0.75
  },

  headerProgressBar: {
    width: '100%',
    marginBottom: 8
  },

  headerProgressTextContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },

  headerProgressCount: {
    color: Colors.WHITE
  },

  headerProgressText: {
    color: Colors.WHITE,
    marginLeft: 6
  }

});

export default TrackScreen;