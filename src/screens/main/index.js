import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { Navigation } from 'utils';
import { useTranslation } from 'locales';

import TabsNavigator from './tabs';
import ComponentsNavigator from './components';
import SettingsScreen from './settings';

import TrackScreen from './track';
import CourseScreen from './course';


const Stack = createStackNavigator();

function MainNavigator() {
  const { t } = useTranslation();

  return (
    <Stack.Navigator screenOptions={Navigation.stackScreenOptions}>
      <Stack.Screen name="Tabs" component={TabsNavigator} />
      <Stack.Screen name="Components" options={Navigation.noHeaderOptions} component={ComponentsNavigator} />
      <Stack.Screen name="Settings" options={{ title: t('screens.main.settings.title') }} component={SettingsScreen} />
      <Stack.Screen name="Course" component={CourseScreen} />
      <Stack.Screen name="Track" component={TrackScreen} />
    </Stack.Navigator>
  );
};

export default MainNavigator;