import { NativeModules, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import i18n from 'i18next';
import { initReactI18next, useTranslation } from 'react-i18next';

import ptBR from './pt_BR';
import es from './es';


let currentLanguage;

const getCurrentLanguage = () => currentLanguage;

const languageDetector = {
  type: 'languageDetector',
  async: true,
  detect: async (callback) => {
    currentLanguage = await AsyncStorage.getItem('current-language');
    if (!currentLanguage)
      currentLanguage = ((Platform.OS == 'android') ?
        NativeModules.I18nManager.localeIdentifier :
        (NativeModules.SettingsManager.settings.AppleLocale || NativeModules.SettingsManager.settings.AppleLanguages[0])
      ).replace('_', '-');

    return callback(currentLanguage);
  },
  init: () => {},
  cacheUserLanguage: (language) => {
    currentLanguage = language;
    AsyncStorage.setItem('current-language', language)
  },
};

const options = {
  resources: {
    'pt-BR': ptBR,
    'es': es
  },
  fallbackLng: 'pt-BR',
  interpolation: {
    escapeValue: false,
  },
};

const init = async () => {
  i18n
  .use(languageDetector)
  .use(initReactI18next)
  .init(options);
};

export default i18n;

export {
  init,
  useTranslation,
  getCurrentLanguage
};