import React from 'react';
import { StyleSheet, View } from 'react-native';

import { useTranslation } from 'locales';

import { Colors } from '@growth/commons/utils';
import { ScreenContainer } from './ScreenContainer';
import { Avatar } from './Avatar';
import { Title, Subtitle, Text } from './Text';
import { Icon } from './Icon';


const ScreenHeader = React.memo(({ style, avatarUri, avatarString, title, subtitle, titleNumberOfLines = 1, subtitleNumberOfLines = 1, contentContainerStyle, children, ...otherProps }) => (
  <View style={{ ...styles.headerContainer, ...style }} { ...otherProps }>
    <Avatar uri={avatarUri} string={avatarString} size={70} color={Colors.WHITE} />
    <View style={styles.headerContentContainer}>
      {!!(title && title.length) && <Title style={styles.headerTitle} numberOfLines={titleNumberOfLines}>{title}</Title>}
      {!!(subtitle && subtitle.length) && <Text style={styles.headerSubtitle} numberOfLines={subtitleNumberOfLines}>{subtitle}</Text>}
      <View style={{ ...styles.headerChildrenContainer, ...contentContainerStyle }}>
        { children }
      </View>
    </View>
  </View>
));

const ScreenHeaderInfo = React.memo(({ style, iconName, iconType = 'regular', text, subtitle, ...otherProps }) => (
  <View style={{ ...styles.infoContainer, ...style }}>
    {!!iconName && <Icon name={iconName} type={iconType} size={20} color={Colors.WHITE} style={styles.infoIcon} />}
    {(text != undefined && text != null) && <Text style={styles.infoText}>{text}</Text>}
    {(subtitle != undefined && subtitle != null) && <Subtitle style={styles.infoText}>{subtitle}</Subtitle>}
  </View>
));

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    paddingHorizontal: ScreenContainer.contentContainerStyle.padding,
    paddingBottom: ScreenContainer.contentContainerStyle.padding,
    backgroundColor: Colors.PRIMARY
  },

  headerContentContainer: {
    flex: 1,
    marginLeft: 12
  },

  headerTitle: {
    color: Colors.WHITE
  },

  headerSubtitle: {
    color: Colors.WHITE
  },

  headerChildrenContainer: {
    marginTop: 12
  },

  infoContainer: {
    flexDirection: 'column',
    alignItems: 'center'
  },

  infoIcon: {
    marginBottom: 6
  },

  infoText: {
    textAlign: 'center',
    color: Colors.WHITE
  }
});

ScreenHeader.Info = ScreenHeaderInfo;

export { ScreenHeader };