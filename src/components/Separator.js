import React, { useMemo } from 'react';
import { StyleSheet, View } from 'react-native';

import { Colors } from '@growth/commons/utils';


const Separator = React.memo(({ style, ...otherProps }) => {
  const viewStyle = useMemo(() => ({ ...styles.separator, ...style }), [style]);

  return (
    <View style={viewStyle} { ...otherProps }/>
  );
});

const styles = StyleSheet.create({
  separator: {
    alignSelf: 'stretch',
    marginVertical: 8,
    borderBottomWidth: 1,
    borderColor: Colors.BLACK_LIGHT,
  },
});

Separator.style = styles.separator;

export { Separator };