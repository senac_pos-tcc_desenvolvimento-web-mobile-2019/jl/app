import React, { useMemo } from 'react';
import { StyleSheet, View, Image } from 'react-native';

import { Colors, Strings } from '@growth/commons/utils';

import { Text } from './Text';


const Avatar = React.memo(({ size = 50, color = Colors.PRIMARY, uri, string, style, ...otherProps }) => {
  const initials = useMemo(() => (
    string ? Strings.initials(string) : ''
  ), [string]);

  const containerStyle = useMemo(() => ({ 
    ...styles.container,
    ...style,
    backgroundColor: color + Colors.OPACITY['15'],
    width: size, height: size,
    borderColor: color,
  }), [size, color, style]);

  const initialsStyle = useMemo(() => ({ 
    color,
    fontSize: size * 0.4,
  }), [color, size]);

  return (
    <View style={containerStyle} { ...otherProps }>
      {!!uri ? (
        <Image source={{ uri }} style={{ width: size, height: size }} />
      ) : (
        <Text weight="bold" style={initialsStyle}>{initials}</Text>
      )}
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 4,
    borderRadius: 999,
    overflow: 'hidden'
  },

  image: {
    width: '100%',

  }
});

export { Avatar };