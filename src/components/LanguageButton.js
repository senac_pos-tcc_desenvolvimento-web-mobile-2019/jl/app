import React, { useMemo, useCallback } from 'react';
import { StyleSheet } from 'react-native';

import { Colors } from '@growth/commons/utils';
import { Button } from './Button';
import { useTranslation, getCurrentLanguage } from 'locales';


const LanguageButton = ({ style, ...props }) => {
  const [t, i18n] = useTranslation();
  const nextLanguage = getCurrentLanguage() == 'pt-BR' ? 'es' : 'pt-BR';

  const buttonStyle = useMemo(() => ({ ...styles.button, ...style }), [style]);

  const handleLanguageChange = useCallback(() => {
    i18n.changeLanguage(nextLanguage)
  }, [nextLanguage]);

  return (
    <Button
      leftIcon="globe-americas"
      title={t('screens.main.settings.action.changeLanguage.' + nextLanguage)}
      style={buttonStyle}
      titleStyle={styles.title}
      onPress={handleLanguageChange}
    />
  );
};

const styles = StyleSheet.create({
  button: {
    width: 'auto',
    alignSelf: 'stretch',
    backgroundColor: Colors.BACKGROUND
  },

  title: {
    color: Colors.PRIMARY,
    fontSize: 18
  },
});

export { LanguageButton };