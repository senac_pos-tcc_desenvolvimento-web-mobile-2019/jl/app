import React, { useMemo } from 'react';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';

import { Colors } from '@growth/commons/utils';


const BaseText = React.memo(({ weight = 'normal', style, onPress, children, ...otherProps }) => {
  const textStyle = useMemo(() => ({
    fontWeight: weight,
    ...styles.baseText,
    ...style
  }), [style]);

  if (onPress)
    return (
      <TouchableOpacity onPress={onPress}>
        <Text style={textStyle} { ...otherProps }>{children}</Text>
      </TouchableOpacity>
    );
  else
    return (
      <Text style={textStyle} { ...otherProps }>{children}</Text>
    );
});

const Subtitle = React.memo(({ weight = 'bold', style, ...otherProps }) => {
  const textStyle = useMemo(() => ({ ...styles.subtitle, ...style }), [style]);

  return (
    <BaseText weight={weight} style={textStyle} { ...otherProps }/>
  );
});

const Title = React.memo(({  weight = 'bold', style, ...otherProps }) => {
  const textStyle = useMemo(() => ({ ...styles.title, ...style }), [style]);

  return (
    <BaseText weight={weight} style={textStyle} { ...otherProps }/>
  );
});

const styles = StyleSheet.create({
  baseText: {
    color: Colors.BLACK,
    fontSize: 16,
  },

  subtitle: {
    fontSize: 20,
    color: Colors.PRIMARY,
  },

  title: {
    fontSize: 24,
  },
});

BaseText.styles = styles.baseText;
Subtitle.styles = styles.subtitle;
Title.styles = styles.title;

export {
  BaseText as Text,
  Subtitle,
  Title
};