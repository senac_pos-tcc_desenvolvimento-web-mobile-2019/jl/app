import React, { useMemo } from 'react';
import { StyleSheet, TouchableOpacity, View, TextInput as Input } from 'react-native';

import { Colors } from '@growth/commons/utils';
import { Icon } from './Icon';


const InputContainer = ({ leftIcon, leftIconType, leftIconColor, leftIconSize, onLeftIconPress, rightIcon, rightIconType, rightIconColor, rightIconSize, onRightIconPress, onPress, hasErrors, disabled, style, renderContent = () => {}, ...otherProps }) => {
  const Container = useMemo(() => !!onPress ? TouchableOpacity : View, [onPress]);

  const containerStyle = useMemo(() => ({
    ...styles.container,
    ...(hasErrors && styles.containerError),
    ...(disabled && styles.containerDisabled),
    ...style
  }), [hasErrors, disabled, style]);

  const textStyle = useMemo(() => ({
    ...styles.text,
    ...(hasErrors && styles.textError),
  }), [hasErrors]);

  const placeholderStyle = useMemo(() => ({
    ...styles.placeholder,
    ...(hasErrors && styles.placeholderError),
  }), [hasErrors]);

  return (
    <Container onPress={onPress} disabled={disabled} style={containerStyle} { ...otherProps }>
      {leftIcon && (
        <Icon
          name={leftIcon}
          type={leftIconType}
          color={leftIconColor || containerStyle.borderColor}
          size={leftIconSize || textStyle.fontSize}
          style={styles.leftIcon}
          onPress={onLeftIconPress}
        />
      )}
      
      <View style={styles.contentContainer}>
        { renderContent({ containerStyle, textStyle, placeholderStyle }) }
      </View>

      {rightIcon && (
        <Icon
          name={rightIcon}
          type={rightIconType}
          color={rightIconColor || containerStyle.borderColor}
          size={rightIconSize || textStyle.fontSize}
          style={styles.rightIcon}
          onPress={onRightIconPress}
        />
      )}
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'stretch',
    borderBottomWidth: 1,
    borderColor: Colors.PRIMARY,
  },

  containerError: {
    borderColor: Colors.ERROR,
  },

  containerDisabled: {
    borderColor: Colors.BLACK_LIGHT,
    opacity: 0.4,
  },

  contentContainer: {
    flex: 1,
  },

  text: {
    fontSize: 16,
    color: Colors.BLACK,
    padding: 0, margin: 0,
  },

  textError: {
    color: Colors.ERROR,
  },

  placeholder: {
    color: Colors.BLACK_LIGHT,
  },

  placeholderError: {
    color: Colors.ERROR_LIGHT,
  },

  leftIcon: {
    paddingRight: 8,
    justifyContent: 'center',
    alignSelf: 'stretch',
  },

  rightIcon: {
    paddingLeft: 8,
    justifyContent: 'center',
    alignSelf: 'stretch',
  },
});

export { InputContainer };