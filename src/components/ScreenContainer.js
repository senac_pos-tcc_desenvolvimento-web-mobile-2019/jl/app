import React, { useMemo } from 'react';
import { useHeaderHeight } from '@react-navigation/stack';
import { Platform, StyleSheet, StatusBar, KeyboardAvoidingView, SafeAreaView, ScrollView } from 'react-native';

import { Colors } from '@growth/commons/utils';
import { ScreenLoader } from './ScreenLoader';


const ScreenContainer = React.memo(({ loading = false, keyboadAvoidable = true, scrollable = true, statusBarStyle = 'light-content', statusBarColor = Colors.PRIMARY, renderHeader = () => {}, style, children, ...otherProps }) => {
  const headerHeight = useHeaderHeight();
  
  const keyboardBehaviour = useMemo(() => Platform.OS == 'ios' ? 'padding' : null, []);
  const keyboardVerticalOffset = useMemo(() => Platform.OS == 'ios' ? headerHeight : undefined, [headerHeight]);
  const contentContainerStyle = useMemo(() => ({ ...styles.contentContainer, ...style }), [style]);

  return (
    <React.Fragment>
      <StatusBar barStyle={statusBarStyle} backgroundColor={statusBarColor} />

      <KeyboardAvoidingView
        style={styles.keyboardAvoidingView}
        enabled={keyboadAvoidable}
        behavior={keyboardBehaviour}
        keyboardVerticalOffset={keyboardVerticalOffset}
      >
        <SafeAreaView style={styles.safeAreaView}>
          { renderHeader() }
          <ScrollView
            { ...otherProps }
            horizontal={false}
            keyboardDismissMode="none"
            keyboardShouldPersistTaps="handled"
            scrollEnabled={scrollable}
            contentContainerStyle={contentContainerStyle}
          >
            { children }
          </ScrollView>
        </SafeAreaView>
      </KeyboardAvoidingView>

      <ScreenLoader visible={loading} />
    </React.Fragment>
  );
});

const styles = StyleSheet.create({
  keyboardAvoidingView: {
    flex: 1,
    backgroundColor: Colors.PRIMARY
  },

  safeAreaView: {
    flex: 1,
  },

  contentContainer: {
    flexGrow: 1,
    backgroundColor: Colors.BACKGROUND,
    padding: 24,
  }
});

ScreenContainer.contentContainerStyle = styles.contentContainer;

export { ScreenContainer };