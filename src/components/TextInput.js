import React from 'react';
import { TextInput as Input } from 'react-native';

import { InputContainer } from './InputContainer';


const TextInput = ({ leftIcon, leftIconType, leftIconColor, onLeftIconPress, rightIcon, rightIconType, rightIconColor, onRightIconPress, hasErrors, disabled, style, editable, ...otherProps }) => {
  return (
    <InputContainer
      leftIcon={leftIcon}
      leftIconType={leftIconType}
      leftIconColor={leftIconColor}
      onLeftIconPress={onLeftIconPress}
      rightIcon={rightIcon}
      rightIconType={rightIconType}
      rightIconColor={rightIconColor}
      onRightIconPress={onRightIconPress}
      disabled={disabled}
      hasErrors={hasErrors}
      style={style}
      renderContent={({ textStyle, placeholderStyle }) => {
        return (
          <Input style={textStyle} editable={!disabled && editable} placeholderTextColor={placeholderStyle.color} { ...otherProps }/>
        );
      }}
    />
  );
};

export { TextInput };