import React from 'react';

import { useCurrentUser } from '@growth/commons/context';

import { Avatar } from './Avatar';


const UserAvatar = React.memo(props => {
  const user = useCurrentUser();

  return (
    <Avatar { ...props } uri={user?.avatarUrl} string={user?.name} />
  );
});

export { UserAvatar };