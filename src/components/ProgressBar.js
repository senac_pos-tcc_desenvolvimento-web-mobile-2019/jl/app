import React from 'react';
import { StyleSheet, View } from 'react-native';

import { Colors } from '@growth/commons/utils';

import { Text } from './Text';


const ProgressBar = React.memo(({ progressPercentage = 0, color = Colors.PRIMARY, labelColor = Colors.WHITE, height = 18, style, ...otherProps }) => (
  <View
    style={{ ...styles.container, ...style, backgroundColor: color + Colors.OPACITY['25'], height }}
    { ...otherProps }
  >
    <View style={{ ...StyleSheet.absoluteFillObject, backgroundColor: color, width: `${progressPercentage}%` }} />
    <Text style={{ ...styles.label, color: labelColor }}>{progressPercentage}%</Text>
  </View>
));

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    alignSelf: 'stretch',
  },

  label: {
    fontSize: 12,
    marginRight: 8,
  },
});

export { ProgressBar };