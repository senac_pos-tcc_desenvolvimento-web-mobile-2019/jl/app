import React, { useMemo, useCallback } from 'react';
import { StyleSheet, View } from 'react-native';

import { useTrackCoursesInfo, useUserTrackProgress } from '@growth/commons/context';
import { Colors } from '@growth/commons/utils';

import { Text } from './Text';
import { Icon } from './Icon';
import { Avatar } from './Avatar';
import { TrainingsList } from './TrainingsList';
import { ProgressBar } from './ProgressBar';

import { useTranslation } from 'locales';


const TracksList = ({ tracks = [], onPress, ...otherProps }) => {
  const { t } = useTranslation();

  const renderTrackCard = useCallback((track, index, style) => (
    <TrackCard key={track.id} track={track} onPress={onPress} style={style} />
  ), []);

  return (
    <TrainingsList data={tracks} emptyMessage={t('models.track.none')} renderItem={renderTrackCard} { ...otherProps } />
  )
};

const TrackCard = ({ track, onPress, style }) => {
  const { t } = useTranslation();
  const { estimatedTime } = useTrackCoursesInfo(track);
  const { pending, done, percentage, status } = useUserTrackProgress(track);

  const handlePress = useCallback(() => {
    if (onPress)
      onPress(track);
  }, []);

  return (
    <TrainingsList.Card
      type={TrainingsList.Card.TRACK}
      status={status}
      onPress={handlePress}
      style={style}
    >
      <View style={styles.trackTitle}>
        <Avatar uri={track.badgeUrl} size={24} />
        <Text weight="bold" style={styles.trackName} numberOfLines={1}>{track.name}</Text>
      </View>

      <ProgressBar style={styles.progressBar} progressPercentage={percentage} />

      <View style={styles.trackIconsContainer}>
        <View style={styles.trackIconContainer}>
          <Icon size={28} name="book-open" color={TRACK_ICON_COLOR} />
          <Text style={styles.trackIconText} numberOfLines={2}>
            {`${t(`models.course.status.pendingCount`, { count: pending.length })}\n${t(`models.course.status.doneCount`, { count: done.length })}`}
          </Text>
        </View>
        <View style={styles.trackIconContainer}>
          <Icon size={28} name="book-open" color={TRACK_ICON_COLOR} />
          <Text style={styles.trackIconText} numberOfLines={2}>
            {`${t(`models.course.estimatedTime`)}\n${t(`models.course.estimatedTimeCount`, { count: estimatedTime })}`}
          </Text>
        </View>
      </View>
    </TrainingsList.Card>
  );
};

const TRACK_ICON_COLOR = Colors.BLACK + Colors.OPACITY['75'];

const styles = StyleSheet.create({
  trackTitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  trackImageContainer: {
    width: 24,
    height: 24,
    borderRadius: 99,
    backgroundColor: Colors.PRIMARY
  },

  trackName: {
    flex: 1,
    color: Colors.PRIMARY,
    marginLeft: 8,
  },

  progressBar: {
    marginVertical: 8
  },

  trackIconsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },

  trackIconContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  trackIconText: {
    color: TRACK_ICON_COLOR,
    fontSize: 12,
    marginLeft: 12,
  },
});

TracksList.Card = TrackCard;

export { TracksList };