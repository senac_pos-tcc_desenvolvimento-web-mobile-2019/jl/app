import React, { useMemo } from 'react';
import { View, TouchableOpacity } from 'react-native';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';

import { Colors } from '@growth/commons/utils';


library.add(far, fas, fab);

const Icon = React.memo(({ name, type = 'solid', size = 16, color = Colors.PRIMARY, onPress, style, ...otherProps }) => {
  const iconPackage = useMemo(() => type == 'brands' ? 'fab' : type == 'regular' ? 'far' : 'fas', [type]);
  const Container = useMemo(() => onPress ? TouchableOpacity : View, [onPress]);

  return (
    <Container style={style} onPress={onPress}>
      <FontAwesomeIcon icon={[iconPackage, name]} color={color} size={size} { ...otherProps } />
    </Container>
  );
});

export { Icon };