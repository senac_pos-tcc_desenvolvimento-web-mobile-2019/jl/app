import React from 'react';
import { StyleSheet, Modal as RNModal, View } from 'react-native';

import { Colors } from '@growth/commons/utils';
import { Subtitle, Text } from './Text';
import { Button } from './Button';


const Modal = ({ title, confirmTitle, confirmDisabled, onConfirm, cancelTitle, onCancel, children, ...otherProps }) => {
  return (
    <RNModal animationType="fade" transparent={true} statusBarTranslucent={true} { ...otherProps }>
      <View style={styles.background}>
        <View style={styles.container}>
          {!!(title && title.length) && (
            <Subtitle style={styles.title}>{title}</Subtitle>
          )}

          {!!children && (
            <View style={styles.contentContainer}>
              {children}
            </View>
          )}

          <Button
            title={confirmTitle}
            disabled={confirmDisabled}
            onPress={onConfirm}
            style={styles.confirm}
          />

          {!!(cancelTitle && cancelTitle.length) && (
            <Text style={styles.cancel} onPress={onCancel}>
              {cancelTitle}
            </Text>
          )}
        </View>
      </View>
    </RNModal>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 24,
    backgroundColor: Colors.BLACK + Colors.OPACITY['70']
  },

  container: {
    width: '100%',
    padding: 24,
    backgroundColor: Colors.WHITE,
    borderRadius: 20,
    alignItems: 'center',
  },

  title: {
    marginBottom: 12,
  },

  contentContainer: {
    alignItems: 'center',
    marginBottom: 12,
  },

  confirm: {
    alignSelf: 'center',
    width: 'auto',
  },

  cancel: {
    alignSelf: 'center',
    marginTop: 12,
  },
});

export { Modal };