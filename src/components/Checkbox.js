import React, { useMemo, useCallback } from 'react';
import { StyleSheet } from 'react-native';

import { InputContainer } from './InputContainer';
import { Text } from './Text';


const Checkbox = ({ value, label, iconSize = 20, onChange, hasErrors, disabled, style, ...otherProps }) => {
  const leftIcon = useMemo(() => value ? 'check-square' : 'square', [value]);
  const containerStyle = useMemo(() => ({ ...styles.container, ...style }), [style]);

  const handlePress = useCallback(() => {
    if (onChange)
      onChange(!value);
  }, [value, onChange]);

  return (
    <InputContainer
      leftIconType="regular"
      leftIconSize={iconSize}
      leftIcon={leftIcon}
      disabled={disabled}
      hasErrors={hasErrors}
      onPress={handlePress}
      style={containerStyle}
      renderContent={({ textStyle }) => {
        return (
          <Text style={textStyle}>{label}</Text>
        );
      }}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 0,
  },
});

export { Checkbox };