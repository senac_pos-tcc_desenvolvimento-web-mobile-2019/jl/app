import React from 'react';
import { StyleSheet, Modal, View, ActivityIndicator } from 'react-native';

import { Colors } from '@growth/commons/utils';


const ScreenLoader = React.memo(({ visible = false, children }) => (
  <Modal
    animationType="fade"
    transparent={true}
    statusBarTranslucent={true}
    visible={visible}
  >
    <View style={styles.container}>
      <ActivityIndicator size="large" color={Colors.PRIMARY} />
    </View>
  </Modal>
));

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.WHITE + Colors.OPACITY['60'],
  },
});

export { ScreenLoader };