import React, { useMemo, useCallback } from 'react';
import { StyleSheet, View } from 'react-native';

import { useUserCourseProgress } from '@growth/commons/context';
import { Colors } from '@growth/commons/utils';

import { Text } from './Text';
import { Avatar } from './Avatar';
import { TrainingsList } from './TrainingsList';

import { useTranslation } from 'locales';


const CoursesList = ({ courses = [], onPress, ...otherProps }) => {
  const { t } = useTranslation();

  const renderCourseCard = useCallback((course, index, style) => (
    <CourseCard key={course.id} course={course} onPress={onPress} style={style} />
  ), []);

  return (
    <TrainingsList data={courses} emptyMessage={t('models.course.none')} renderItem={renderCourseCard} { ...otherProps } />
  )
};

const CourseCard = ({ course, onPress, style }) => {
  const { t } = useTranslation();
  const { status } = useUserCourseProgress(course);

  const handlePress = useCallback(() => {
    if (onPress)
      onPress(course);
  }, []);

  return (
    <TrainingsList.Card
      type={TrainingsList.Card.COURSE}
      status={status}
      onPress={handlePress}
      style={style}
      contentContainerStyle={styles.cardContent}
    >
      <Avatar uri={course.badgeUrl} size={35} />
      <View style={styles.courseTextsContainer}>
        <Text weight="bold" style={styles.courseName} numberOfLines={1}>{course.name}</Text>
        <Text numberOfLines={2}>{course.description}</Text>
        {status && <Text style={styles.courseStatus}>{t('models.course.status.' + status)}</Text>}
      </View>
    </TrainingsList.Card>
  );
};

const styles = StyleSheet.create({
  cardContent: {
    flexDirection: 'row',
  },

  courseTextsContainer: {
    flex: 1,
    marginLeft: 8,
  },

  courseName: {
    color: Colors.PRIMARY
  },

  courseStatus: {
    alignSelf: 'flex-end',
    color: Colors.BLACK_LIGHT,
    marginTop: 4,
  }
});

CoursesList.Card = CourseCard;

export { CoursesList };