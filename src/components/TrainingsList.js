import React, { useMemo } from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';

import { Colors } from '@growth/commons/utils';

import { Subtitle, Text } from './Text';
import { Icon } from './Icon';

import { useTranslation } from 'locales';


const COURSE = 'course';
const TRACK = 'track';

const TrainingsList = React.memo(({ title, data = [], renderItem = () => {}, emptyMessage, ...otherProps }) => {
  const { t } = useTranslation();

  return (
    <View { ...otherProps }>
      { !!(title && title.length) && <Subtitle style={styles.listTitle}>{title}</Subtitle> }

      { !!data.length && data.map((item, index) => (
        renderItem(item, index, index && styles.listCard)
      )) }

      { !data.length && !!emptyMessage && (
        <Text style={styles.listEmptyMessage}>{emptyMessage}</Text>
      ) }
    </View>
  )
});

const TrainingCard = ({ type, status, onPress, style, contentContainerStyle, children }) => {
  const { t } = useTranslation();

  const cardStyle = useMemo(() => ({ ...styles.container, ...style}), [style]);
  const iconProps = useMemo(() => ({
    size: status == 'done' && type == TRACK ? 22 : 18,
    name: status == 'done' ? (type == TRACK ? 'graduation-cap' : 'medal') : 'bookmark',
    color: status == 'done' ? Colors.PRIMARY : Colors.BLACK_LIGHT,
    style: { ...styles.statusIcon, ...(status == 'done' && type == TRACK && { top: 4 } ) }
  }), [status]);

  return (
    <TouchableOpacity style={cardStyle} onPress={onPress}>
      <Text style={styles.cardType}>{t(`models.${type}.label`)}</Text>
      {!!status && (type == COURSE || type == TRACK) && (
        <Icon { ...iconProps } />
      )}

      <View style={contentContainerStyle}>
        { children }
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  listTitle: {
    marginBottom: 8,
  },

  listEmptyMessage: {
    alignSelf: 'center',
    padding: 12,
  },

  listCard: {
    marginTop: 12,
  },

  container: {
    backgroundColor: Colors.WHITE,
    borderLeftWidth: 5,
    borderColor: Colors.PRIMARY,
    padding: 8,
  },

  cardType: {
    color: Colors.PRIMARY,
    marginBottom: 6,
  },

  statusIcon: {
    position: 'absolute',
    top: 0,
    right: 16,
  },
});

TrainingCard.COURSE = COURSE;
TrainingCard.TRACK = TRACK;
TrainingsList.Card = TrainingCard;

export { TrainingsList };