import React, { useMemo } from 'react';
import { Dimensions, StyleSheet, TouchableOpacity } from 'react-native';

import { Colors } from '@growth/commons/utils';

import { Subtitle } from './Text';
import { Icon } from './Icon';


const { width } = Dimensions.get('window');

const Button = React.memo(({ title, disabled, style, titleStyle, leftIcon, rightIcon, iconType, iconSize, ...otherProps }) => {
  const containerComponentStyle = useMemo(() => ({
    width: (width * 0.65),
    ...styles.container,
    ...style,
    ...(disabled && styles.disabled)
  }), [style, disabled]);
  const titleComponentStyle = useMemo(() => ({ ...styles.title, ...titleStyle }), [titleStyle]);

  return (
    <TouchableOpacity style={containerComponentStyle} disabled={disabled} { ...otherProps }>
      { !!(leftIcon && leftIcon.length) && <Icon name={leftIcon} color={titleComponentStyle.color} type={iconType} size={iconSize || titleComponentStyle.fontSize} /> }
      { !!(title && title.length) && <Subtitle style={titleComponentStyle}>{title}</Subtitle> }
      { !!(rightIcon && rightIcon.length) && <Icon name={rightIcon} color={titleComponentStyle.color} type={iconType} size={iconSize || titleComponentStyle.fontSize} /> }
    </TouchableOpacity>
  );
});

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY,
    borderRadius: 99,
    paddingVertical: 8,
    paddingHorizontal: 16,
  },

  disabled: {
    opacity: 0.4,
  },

  title: {
    fontSize: Subtitle.styles.fontSize,
    color: Colors.WHITE,
    textAlign: 'center',
    marginHorizontal: 8,
  },
});

export { Button };