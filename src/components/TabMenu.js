import React, { useMemo, useCallback } from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';

import { Colors } from '@growth/commons/utils';

import { Text } from './Text';
import { Icon } from './Icon';


const TabMenu = React.memo(({ data = [], labelExtractor, isSelected, onPress, ...otherProps }) => (
  <View style={styles.container} { ...otherProps }>
    {data.map((item, index, array) => (
      <Tab
        key={index}
        item={item}
        onPress={onPress}
        label={labelExtractor ? labelExtractor(item, index, array) : (item.label || item)}
        selected={isSelected ? isSelected(item, index, array) : item.isSelected}
      />
    ))}
  </View>
));

const Tab = ({ item, label, selected, onPress, ...otherProps }) => {
  const tabStyle = useMemo(() => ({
    ...styles.tabContainer,
    ...(selected && styles.tabContainerSelected) 
  }), [selected]);

  const handleTabPress = useCallback(() => {
    if (onPress)
      onPress(item);
  }, []);

  return (
    <TouchableOpacity style={tabStyle} onPress={handleTabPress}>
      <Text weight="bold" style={styles.tabLabel}>{label}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY,
  },

  tabContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 12,
    borderBottomWidth: 4,
    borderColor: Colors.PRIMARY,
  },

  tabContainerSelected: {
    borderColor: Colors.PRIMARY_DARK,
    backgroundColor: Colors.WHITE + Colors.OPACITY['30']
  },

  tabLabel: {
    color: Colors.WHITE,
  }
});

export { TabMenu };